from django.urls import path,include
from rest_framework.routers import  SimpleRouter
from comment import views

router = SimpleRouter()
router.register(r'comment', views.CommentViewSet)
urlpatterns = router.urls