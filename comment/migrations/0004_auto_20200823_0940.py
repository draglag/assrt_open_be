# Generated by Django 3.0.8 on 2020-08-23 09:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('comment', '0003_auto_20200823_0928'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='assrt_id',
            new_name='assrt',
        ),
    ]
