from django.test import TestCase
from assrt.models import Assrt
from comment.models import Comment
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from django.db.utils import IntegrityError

class test_CommentTestCase(TestCase):

    def setUp(self):
        user1 = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user1.save()
        user2 = User.objects.create_user(username='miao', email='miao@miao.com', password='miao')
        user2.save()
        ins = Assrt.objects.create(text_assrt="silvio e corrotto", author=user1)
        ins.save()
        self.ins_id=ins.id
        ins2 = Assrt.objects.create(text_assrt="qualcuno e pulito", author=user2)
        ins2.save()
        user = User.objects.get(username='babu')
        Token.objects.create(user=user)
    """
    def test_comment_insert(self):
        user = User.objects.get(username='babu')
        assrt = Assrt.objects.get(slug="silvio-e-corrotto")
        comment = Comment(author=user,
                         assrt=assrt,
                         text_comment="commento di test",
                         link="http://www.google.it",
                         meta="meta descritpion",
                         yn=False)
        comment.save()
        ins_comm = Comment.objects.get(id = self.ins_id)
        assert ins_comm.assrt.id == assrt.id
    
    def test_comment_insert_no_link(self):
        user = User.objects.get(username='babu')
        assrt = Assrt.objects.get(slug="silvio-e-corrotto")
        comment = Comment(author=user,
                         assrt=assrt,
                         text_comment="",
                         link="",
                         meta="meta descritpion",
                         yn=False)
        comment.save()
        ins_comm = Comment.objects.get(id = self.ins_id)
        assert ins_comm.assrt.id == assrt.id
    """

