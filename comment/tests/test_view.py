from assrt.models import Assrt
from comment.models import Comment
from rest_framework.test import  APIClient, APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from assrt_be import settings

class WithoutConnTestCase(APITestCase):

    def setUp(self):
        user1 = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user1.save()
        user2 = User.objects.create_user(username='miao', email='miao@miao.com', password='miao')
        user2.save()
        ins=Assrt.objects.create(text_assrt= "silvio e corrotto", author = user1)
        ins.save()
        ins2=Assrt.objects.create(text_assrt= "nuova idea da testare", author = user2)
        ins2.save()
        self.assrt_id = str(ins2.id)
        comment = Comment.objects.create(author=user1,
                                         assrt=ins,
                                         text_comment="commento di test",
                                         link="http://www.google.it",
                                         meta="meta descritpion",
                                         yn=False)
        comment.save()
        user = User.objects.get(username='babu')
        Token.objects.create(user=user)
        user = User.objects.get(username='babu')
        token = Token.objects.get(user=user)
        auth_string = " Token {}".format(token.key)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=auth_string)
        user2 = User.objects.get(username='miao')
        Token.objects.create(user=user2)
        token2 = Token.objects.get(user=user2)
        auth_string2 = " Token {}".format(token2.key)
        self.client2 = APIClient()
        self.client2.credentials(HTTP_AUTHORIZATION=auth_string2)

    def test_comment_link_valid(self):
        assrt = Assrt.objects.get(slug="nuova-idea-da-testare")
        url = 'http://localhost:8000/comment/'
        data = {'assrt':assrt.id,
                'text_comment': 'maveramente cattivoasdadadad',
                'link': 'https://stackoverflow.com/',
                'yn': 'False'}
        response2 = self.client.post(url, data, format='json')
        response3 = self.client.get('http://localhost:8000/assrt/'+self.assrt_id+'/', format='json')
        assert response2.status_code == 201  # post commment ok
        assert len(response3.data['comments']) != 0  # comments exists in new assrt


    def test_comment_no_link(self):
        assrt = Assrt.objects.get(slug="nuova-idea-da-testare")
        url = 'http://localhost:8000/comment/'
        data = {'assrt': assrt.id,
                'text_comment': 'test no link',
                'link': '',
                'yn': 'False'}
        response = self.client.post(url, data, format='json')
        response3 = self.client.get('http://localhost:8000/assrt/'+str(assrt.id)+'/', format='json')
        assert response.status_code == 400


    def test_comment_no_commment(self):
        assrt = Assrt.objects.get(slug="nuova-idea-da-testare")
        url = 'http://localhost:8000/comment/'
        data = {'assrt': assrt.id,
                'text_comment': '',
                'link': 'https://stackoverflow.com/',
                'yn': 'False'}
        response = self.client.post(url, data, format='json')
        assert response.status_code == 201

    def test_unauthorized(self):

        client = APIClient()
        url = 'http://localhost:8000/comment/nuova-idea-da-testare/'
        data = {'text_comment': 'maveramente cattivoasdadadad',
                'link': 'https://stackoverflow.com/',
                'yn': 'False'}
        response2 = client.post(url, data, format='json')
        assert response2.status_code == 401  # post commment ok


    def test_update(self):
        comment = Comment.objects.first()
        url = 'http://localhost:8000/comment/'+ str(comment.id) +'/'
        data = {'text_comment': 'test comment update',
                'yn': False}
        response2 = self.client.put(url, data, format='json')
        assert response2.status_code == 201  # post commment ok

    def test_update_author_error(self):
        comment = Comment.objects.first()
        url = 'http://localhost:8000/comment/'+ str(comment.id) +'/'
        data = {'text_comment': 'test comment update',
                'yn': False}
        response2 = self.client2.put(url, data, format='json')
        assert response2.status_code == 400  # post commment ok

    def test_update_author_error(self):
        comment = Comment.objects.first()
        url = 'http://localhost:8000/comment/' + str(comment.id) + '/'
        response2 = self.client2.delete(url)
        assert response2.status_code == 400  # post commment ok

    def test_delete(self):
        comment = Comment.objects.first()
        url = 'http://localhost:8000/comment/' + str(comment.id) + '/'
        response = self.client.delete(url)
        assert response.status_code == 201  # post commment ok

"""
class WithConnTestCase(APITestCase):

    def test_comment_ok(self):
        if s.FIREWALL:
            pass
        else:
            #insert assrt
            url1 = 'http://localhost:8000/assrt/'
            data1 = {'text_assrt': 'nuova idea da testare'}
            response = self.client.post(url1, data1, format='json')
            response4 = self.client.get('http://localhost:8000/assrt/nuova-idea-da-testare/', format=json)
            url = 'http://localhost:8000/comment/'
            data = {'text_comment': 'maveramente cattivoasdadadad',
                    'slug': 'nuova-idea-da-testare',
                    'link': 'https://stackoverflow.com/',
                    'yn': 'False'}
            response2 = self.client.post(url,data, format= 'json')
            response3 = self.client.get('http://localhost:8000/assrt/nuova-idea-da-testare/',  format='json')
            assert response.status_code == 201 # assrt created
            assert response4.status_code == 200 # new assrt exists
            assert response2.status_code == 201 # post commment ok
            assert len(response3.data['comments']) > 0 # comments exists in new assrt


    def test_comment_no_meta(self):
        if s.FIREWALL:
            pass
        else:
             url1 = 'http://localhost:8000/assrt/'
             data1 = {'text_assrt': 'nuova idea da testare'}
             response1 = self.client.post(url1, data1 ,format = 'json')
             url = 'http://localhost:8000/comment/'
             data = {'slug':'nuova-idea-da-testare',
                      'text_comment':'ma ma m averamente cattivoasdadadad',
                      'link':'https://localhost/',
                      'yn':'False'}
             response = self.client.post(url, data, format='json')
             response3= self.client.get('http://localhost:8000/assrt/nuova-idea-da-testare/',  format='json')
             assert response.status_code == 202

    def test_comment_no_comment(self):
        if s.FIREWALL:
            pass
        else:
            url1 = 'http://localhost:8000/assrt/'
            data1 = {'text_assrt': 'nuova idea da testare'}
            response1 = self.client.post(url1, data1 ,format = 'json')
            url = 'http://localhost:8000/comment/'
            data = {'slug':'nuova-idea-da-testare',
                  'text_comment':'',
                  'link':'http://localhost/',
                  'yn':'False'}
            response = self.client.post(url, data, format = 'json')
            response3= self.client.get('http://localhost:8000/assrt/nuova-idea-da-testare/',  format='json')
            assert response.status_code == 201


    def test_post_comment_no_valid_meta(self):
        if s.FIREWALL:
            pass
        else:
            url1 = 'http://localhost:8000/assrt/'
            data1 = {'text_assrt': 'new idea'}
            response1 = self.client.post(url1, data1 ,format = 'json')
            url = 'http://localhost:8000/comment/'
            data = {'slug':'new-idea',
                    'text_comment':'ma ma m averamente cattivoasdadadad',
                    'link':'http://www.google.it',
                    'yn':'False'}
            response = self.client.post(url, data, format = 'json')
            assert response.status_code == 201

    def test_post_comment_valid_meta(self):
        if s.FIREWALL:
            pass
        else:
            url1 = 'http://localhost:8000/assrt/'
            data1 = {'text_assrt': 'new idea'}
            response1 = self.client.post(url1, data1 ,format = 'json')
            url = 'http://localhost:8000/assrt/comment/'
            data = {'slug':'new-idea',
                    'text_comment':'ma ma m averamente cattivoasdadadad',
                    'link':'https://www.ilsole24ore.com/',
                    'yn':'False'}
            response = self.client.post(url, data, format = 'json')
            assert response.status_code == 201
"""