from rest_framework import serializers
from comment.models import Comment
from like.serializers import LikeInCommentSerializer
from reply.serializers import ReplyInCommenmentSerializer
from aut.serializers import JustUsernameSerializer

class CommentInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ('assrt','text_comment', 'link', 'yn' )

class CommentSerializer(serializers.ModelSerializer):
    likes = LikeInCommentSerializer(many=True, required=False)

    class Meta:
        model = Comment
        fields = ('assrt','text_comment', 'link', 'yn', 'logdatum','likes')

class CommentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('text_comment', 'yn')

class CommentInAssrtSerializer(serializers.ModelSerializer):
    likes = LikeInCommentSerializer(many=True, required=False)
    reply = ReplyInCommenmentSerializer(many=True, required = False)
    user_data = serializers.SerializerMethodField()

    def get_user_data(self, obj):
        user = JustUsernameSerializer(obj.author, many=False).data
        return user

    class Meta:
        model = Comment
        fields = ('id','assrt_id', 'text_comment', 'link','meta', 'yn', 'user_data', 'logdatum', 'likes', 'reply')

class CommentCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ['yn', ]