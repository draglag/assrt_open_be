from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from rest_framework import viewsets
from comment.models import Comment
from assrt.models import Assrt
from comment.serializers import CommentSerializer, CommentUpdateSerializer, CommentInAssrtSerializer, CommentInsertSerializer
import metadata_parser
import requests
import logging
from drf_yasg.utils import swagger_auto_schema

logger = logging.getLogger(__name__)

# Create your views here.
class CommentViewSet(viewsets.ViewSet):
    """
    retrieve:
    Return the given comment.

    create:
    Insert a new comment.

    update:
    Update a commment.

    destroy:
    delete a comment.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentInsertSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = Comment.objects.all()
        comments = get_object_or_404(queryset, id=pk)
        serializer = CommentInAssrtSerializer(comments)
        return Response(serializer.data)

    @swagger_auto_schema(operation_description="POST /comment/", request_body=CommentInsertSerializer)
    def create(self, request):
        data = JSONParser().parse(request)
        serializer = CommentInsertSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                try:
                    r = requests.get(data['link'])
                    page = metadata_parser.MetadataParser(url=data['link'], search_head_only=True)
                    if page.get_metadatas('description') is None:
                        hearders = {'headers': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0'}
                        n = requests.get(data['link'],headers=hearders)
                        al = n.text
                        title = al[al.find('<title>') + 7: al.find('</title>')]
                        meta_desc = title
                    else:
                        meta_desc = page.get_metadatas('description')#[0]
                except Exception as e:
                    logger.exception(e)
                    return JsonResponse("It was not possible to proof your link,please retry", status=302, safe=False)
                assrt = Assrt.objects.get(id = data['assrt'])
                comment = Comment(author = request.user,
                                  assrt = assrt,
                                  text_comment=data['text_comment'],
                                  link=data['link'],
                                  meta=meta_desc,
                                  yn=data['yn'])
                comment.save()
                return JsonResponse({'id': comment.id}, status=201, safe=False)
            return JsonResponse(serializer.errors, status=400)

    @swagger_auto_schema(operation_description="UPDATE /comment/{id}/", request_body=CommentUpdateSerializer)
    def update(self, request, pk=None):
        data = JSONParser().parse(request)
        serializer = CommentUpdateSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                comment = Comment.objects.get(id=pk)
                if comment.author == request.user:
                    comment.text_comment = data['text_comment']
                    comment.yn = data['yn']
                    comment.save(update_fields=['text_comment', 'yn'])
                    return JsonResponse("comment updated correctly", status=201, safe=False)
                else:
                    return JsonResponse("author error", status=400, safe=False)

    def destroy(self, request, pk=None):
        if request.user.is_authenticated:
            comment = Comment.objects.get(id=pk)
            #print("{0}--{1}".format(comment.author, request.user))
            if comment.author == request.user:
                comment.delete()
                return JsonResponse("comment deleted correctly", status=201, safe=False)
            else:
                return JsonResponse("author error", status=400, safe=False)