from django.db import models
from assrt.models import Assrt
from django.conf import settings
# Create your models here.
class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE )
    assrt = models.ForeignKey(Assrt, null = True, related_name= 'comments', on_delete=models.CASCADE)
    text_comment = models.CharField(max_length=200, blank=True)
    link = models.CharField(max_length=500)
    yn = models.BooleanField()
    meta = models.CharField(max_length=1000, blank=True)
    approved = models.BooleanField(default=True)
    logdatum = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-logdatum']

#    def __str__(self):
#        return ('{0}-{1}-{2}-{3}-{4}').format(self.author, self.assrt, self.slug, self.text_comment, self.meta)