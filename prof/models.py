from django.db import models
from django.contrib.auth.models import User


class Prof(models.Model):
    author = models.OneToOneField(User, related_name='author', on_delete=models.CASCADE, )
    bio = models.CharField(max_length=250)
    country = models.CharField(max_length=100)
    town = models.CharField(max_length=100)
    bday = models.DateField()
    reputation=models.FloatField(null=True)

class Photo(models.Model):
    owner = models.OneToOneField(User, related_name='owner',  on_delete=models.CASCADE, )
    image = models.BinaryField(max_length=350, blank=True) #max 25kb + 33% base64 encoding
    #image = models.FileField(blank=False, null=False)
