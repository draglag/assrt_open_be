from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.parsers import FileUploadParser
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from prof.models import Prof, Photo
from prof.serializers import ProfSerializer, ProfInsertSerializer, PhotoSerializer#, ProfWebSerializer
from aut.serializers import UsernameSerializer
import logging
from drf_yasg.utils import swagger_auto_schema
from django.contrib.auth.models import User

logger = logging.getLogger(__name__)


# Create your views here.
class ProfViewSet(viewsets.ViewSet):
    """
    retrieve:
    Return the given profile.

    create:
    Insert a new profile.

    update:
    Update a profile.

    destroy:
    delete a profile.
    """
    queryset = Prof.objects.all()
    #lookup_field = 'author'
    serializer_class = ProfSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = Prof.objects.all()
        profile = get_object_or_404(queryset, author=pk)
        serializer = ProfSerializer(profile)
        return Response(serializer.data)

    @swagger_auto_schema(operation_description="POST /profile/", request_body=ProfInsertSerializer)
    def create(self, request):
        data = JSONParser().parse(request)
        serializer = ProfInsertSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                prof = Prof(author = request.user,
                            bio = data['bio'],
                            country=data['country'],
                            town=data['town'],
                            bday = data['bday'],
                            )
                prof.save()
                return JsonResponse({'id': prof.id}, status=201, safe=False)
            return JsonResponse(serializer.errors, status=400)

    @swagger_auto_schema(operation_description="UPDATE /profile/{id}/", request_body=ProfInsertSerializer)
    def update(self, request, pk=None):
        data = JSONParser().parse(request)
        serializer = ProfInsertSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                prof = Prof.objects.get(author=pk)
                if prof.author == request.user:
                    prof.bio = data['bio'],
                    prof.country = data['country'],
                    prof.town = data['town'],
                    prof.bday = data['bday']
                    prof.save(update_fields=['bio', 'country', 'town', 'bday'])
                    return JsonResponse({'id': prof.id}, status=201, safe=False)
                else:
                    return JsonResponse("author error", status=400, safe=False)

    def destroy(self, request, pk=None):
        if request.user.is_authenticated:
            prof = Prof.objects.get(author=pk)
            #print("{0}--{1}".format(comment.author, request.user))
            if prof.author == request.user:
                prof.delete()
                return JsonResponse("status ok", status=201, safe=False)
            else:
                return JsonResponse("error", status = 400, safe= False)

class PhotoViewSet(viewsets.ViewSet):
    queryset = Photo.objects.all()
    #lookup_field = 'owner'
    parser_classes = [FileUploadParser,]
    serializer_class = PhotoSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = Photo.objects.all()
        photo = get_object_or_404(queryset, owner=pk)
        serializer = PhotoSerializer(photo)
        return Response(serializer.data)

    @swagger_auto_schema(operation_description="POST /photo/", request_body=PhotoSerializer)
    def create(self, request):
        data = JSONParser().parse(request)
        serializer = PhotoSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                photo = Photo(owner = request.user,
                            image = data['image'],
                            )
                photo.save()
                return JsonResponse({'id': photo.id}, status=201, safe=False)
            return JsonResponse(serializer.errors, status=400)

    @swagger_auto_schema(operation_description="UPDATE /photo/{id}", request_body=PhotoSerializer)
    def update(self, request, pk=None):
        data = JSONParser().parse(request)
        serializer = PhotoSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                photo = Photo.objects.get(owner=pk)
                if photo.owner == request.user:
                    photo.image = data['image'],
                    return JsonResponse({'id': photo.id}, status=201, safe=False)
                else:
                    return JsonResponse("author error", status=400, safe=False)

    def destroy(self, request, pk=None):
        if request.user.is_authenticated:
            photo = Photo.objects.get(owner=pk)
            if photo.owner == request.user:
                photo.delete()
                return JsonResponse("ok", status=201, safe=False)
            else:
                return JsonResponse("error", status = 400, safe= False)

class ProfWebViewSet(viewsets.ViewSet):
    """
    retrieve:
    Return the given profile.

    """
    queryset = Prof.objects.all()
    lookup_field = 'username'
    serializer_class = ProfSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, username=None):
        queryset2= User.objects.all()
        user = get_object_or_404(queryset2, username=username)
        queryset = Prof.objects.all()
        profile = get_object_or_404(queryset, author=user.id)
        serializer = ProfSerializer(profile)
        return Response(serializer.data)