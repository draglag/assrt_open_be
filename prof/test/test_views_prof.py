from prof.models import Prof
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from datetime import date

class ProfTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user.save()
        prof = Prof(author=user,
                    bio="qualcosa sull utente",
                    country='italia',
                    town= "roma",
                    bday=date(2020, 10, 10))
        prof.save()
        self.prof_id = str(prof.id)
        #self.prof_id = 'babu'
        Token.objects.create(user=user)
        user = User.objects.get(username='babu')
        token = Token.objects.get(user=user)
        auth_string = " Token {}".format(token.key)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=auth_string)

    def test_prof(self):
        url = 'http://localhost:8000/profile/'+ self.prof_id +'/'
        response = self.client.get(url,format='json')
        assert response.status_code == 200  # post commment ok

    def test_prof_no_auth(self):
        client = APIClient()
        url = 'http://localhost:8000/profile/'+ self.prof_id +'/'
        response = client.get(url,format='json')
        assert response.status_code == 200  # post commment ok

    def test_prof_update(self):
        url = 'http://localhost:8000/profile/'+ self.prof_id +'/'
        data= {'bio':'qualcosaltor',
               'country': 'bau',
               'town': 'viggiu',
               'bday':'2020-09-09' }
        response = self.client.put(url,data = data, format='json')
        assert response.status_code == 201  # post commment ok

    def test_prof_update_no_auth(self):
        client = APIClient()
        url = 'http://localhost:8000/profile/'+ self.prof_id +'/'
        data= {'bio':'qualcosaltor',
               'country': 'bau',
               'town': 'viggiu',
               'bday':'2020-09-09' }
        response = client.put(url,data = data, format='json')
        assert response.status_code == 401  # post commment ok

    def test_prof_delete(self):
        url = 'http://localhost:8000/profile/'+ self.prof_id +'/'
        response = self.client.delete(url,format='json')
        assert response.status_code == 201  # post commment ok

    def test_prof_delete_no_auth(self):
        client = APIClient()
        url = 'http://localhost:8000/profile/'+ self.prof_id +'/'
        response = client.delete(url,format='json')
        assert response.status_code ==  401  # post commment ok

