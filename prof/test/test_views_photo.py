from prof.models import Photo
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from datetime import date
import os
import base64

class PhotoTestCase(APITestCase):

    def setUp(self):
        #print( os.path.dirname(os.path.realpath(__file__)))
        user = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user.save()
        self.user_id = str(user.id)
        #self.user_id = 'babu'
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),"photo.png"), 'rb') as file:
            image =  base64.b64decode(file.read())

        photo = Photo(owner=user,
                    image=image)
        photo.save()
        self.photo_id = str(photo.id)
        Token.objects.create(user=user)
        user = User.objects.get(username='babu')
        token = Token.objects.get(user=user)
        auth_string = " Token {}".format(token.key)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=auth_string)

    def test_prof(self):
        url = 'http://localhost:8000/photo/'+self.user_id+'/'
        response = self.client.get(url,format='json')
        assert response.status_code == 200  # post commment ok

    def test_prof_no_auth(self):
        client = APIClient()
        url = 'http://localhost:8000/photo/'+self.user_id+'/'
        response = client.get(url,format='json')
        assert response.status_code == 200  # post commment ok

    def test_prof_update(self):

        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),"photo.png"), 'rb') as file:
            image = base64.b64decode(file.read())
            #print(image)
        url = 'http://localhost:8000/photo/'+self.user_id+'/'
        data= {'image':str(image)}
        response = self.client.put(url,data = data, format='json')
        assert response.status_code == 201  # post commment ok

    def test_prof_update_no_auth(self):
        client = APIClient()
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),"photo.png"), 'rb') as file:
            image = base64.b64decode(file.read())
        url = 'http://localhost:8000/photo/'+self.user_id+'/'
        data= {'image':str(image)}
        response = client.put(url,data = data, format='json')
        assert response.status_code == 401  # post commment ok

    def test_prof_delete_no_auth(self):
        client = APIClient()
        url = 'http://localhost:8000/photo/'+self.user_id+'/'
        response = client.delete(url,format='json')
        assert response.status_code ==  401  # post commment ok

    def test_prof_delete(self):
        url = 'http://localhost:8000/photo/'+self.user_id+'/'
        response = self.client.delete(url,format='json')
        assert response.status_code == 201  # post commment ok

    def test_upload_photo_bigger_than_25k(self):
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),"photo_big.jpg"), 'rb') as file:
            image = base64.b64decode(file.read())
        url = 'http://localhost:8000/photo/'+self.user_id+'/'
        data= {'image':str(image)}
        #print(len(str(image)))
        response = self.client.put(url,data = data, format='json')
        assert response.status_code == 201  # post commment ok
