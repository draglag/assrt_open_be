from rest_framework.routers import SimpleRouter
from prof import views

router = SimpleRouter()
router.register(r'profile', views.ProfViewSet)
router.register(r'photo', views.PhotoViewSet)
router.register(r'web/profile', views.ProfWebViewSet)
urlpatterns = router.urls