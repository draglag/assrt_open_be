# Generated by Django 3.0.8 on 2020-11-09 07:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('prof', '0009_auto_20201103_1610'),
    ]

    operations = [
        migrations.AddField(
            model_name='prof',
            name='reputation',
            field=models.FloatField(null=True),
        ),
    ]
