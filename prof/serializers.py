from rest_framework import serializers
from assrt.serializers import AssrtSerializer
from prof.models import Prof, Photo
from aut.serializers import UsernameSerializer
from follow.serializers import FollowAssrtSerializer, FollowUserSerializer
from django.contrib.auth.models import User

class ProfSerializer(serializers.ModelSerializer):
    author = UsernameSerializer()
    assrts = AssrtSerializer(many=True, required=False)

    class Meta:
        model = Prof
        fields = ['id', 'bio', 'country', 'town', 'bday', 'assrts', 'author']


class ProfInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Prof
        fields = ['bio', 'country', 'town', 'bday']

class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ['image',]



