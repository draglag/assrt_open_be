from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from rest_framework.decorators import action
from rest_framework import viewsets
from comment.models import Comment
from like.models import Like
from like.serializers import LikeInsertSerializer, LikeSerializer
import logging
from drf_yasg.utils import swagger_auto_schema

logger = logging.getLogger(__name__)

class LikeViewSet(viewsets.ViewSet):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = Like.objects.all()
        like = get_object_or_404(queryset, id=pk)
        serializer = LikeSerializer(like)
        return Response(serializer.data)

    @swagger_auto_schema(operation_description="POST /like/", request_body=LikeInsertSerializer)
    def create(self, request):
        data = JSONParser().parse(request)
        serializer = LikeInsertSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid() :
                comment = Comment.objects.get(id=data['comment'])
                if Like.objects.filter(author=request.user, comment=comment).count() == 0:
                    like = Like(author=request.user,
                                comment=comment,
                                like=data['like'],
                                )
                    like.save()

                    return JsonResponse({'like_id': like.id}, status=201, safe=False)
                return JsonResponse({'error':'already liked this comment'}, status=202, safe=False)
            return JsonResponse(serializer.errors, status=400)

    def update(self, request, pk=None):
        data = JSONParser().parse(request)
        serializer = LikeSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                like = Like.objects.get(id=pk)
                if like.author == request.user:
                    like.like = data['like'],
                    return JsonResponse("ok", status=201, safe=False)
                else:
                    return JsonResponse("author error", status=400, safe=False)

    def destroy(self, request, pk=None):
        if request.user.is_authenticated:
            like = Like.objects.get(id=pk)
            if like.author == request.user:
                like.delete()
                return JsonResponse("ok", status=201, safe=False)
            else:
                return JsonResponse("error", status=400, safe=False)