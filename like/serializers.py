from rest_framework import serializers
from like.models import Like
from aut.serializers import JustUsernameSerializer

class LikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('like',)

class LikeInCommentSerializer(serializers.ModelSerializer):
    user_data = serializers.SerializerMethodField()

    def get_user_data(self, obj):
        user = JustUsernameSerializer(obj.author , many=False).data
        return user

    class Meta:
        model = Like
        fields = ('author', 'like', 'user_data')


class LikeInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Like
        fields = ('comment','like')
