from django.db import models
from django.conf import settings
from comment.models import Comment
from assrt.models import Assrt
# Create your models here.
class Like(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='like_user' , on_delete=models.CASCADE, )
    comment = models.ForeignKey(Comment, related_name='likes', on_delete=models.CASCADE)
    like = models.BooleanField()
    logdatum = models.DateTimeField(auto_now_add=True)
