from rest_framework.routers import SimpleRouter
from like import views

router = SimpleRouter()
router.register(r'like', views.LikeViewSet)
urlpatterns = router.urls
