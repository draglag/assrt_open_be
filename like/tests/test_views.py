from like.models import Like
from assrt.models import Assrt
from comment.models import Comment
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class LikeTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user.save()
        ins=Assrt.objects.create(text_assrt= "silvio e corrotto", author = user)
        ins.save()
        self.assrt_id = ins.id
        comment = Comment(author=user,
                          assrt=ins,
                          text_comment='commmento like',
                          link= "www.google.it",
                          meta="meta desc",
                          yn=True)
        comment.save()
        like= Like(
            author= user,
            comment=comment,
            like= True
                )
        like.save()
        Token.objects.create(user=user)
        user = User.objects.get(username='babu')
        token = Token.objects.get(user=user)
        auth_string = " Token {}".format(token.key)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=auth_string)

    def test_like_202(self):
        comment = Comment.objects.first()
        url = 'http://localhost:8000/like/'
        data = {'comment':comment.id , 'like': False}
        response2 = self.client.post(url, data, format='json')
        assert response2.status_code == 202  # post commment ok

    def test_like_no_auth(self):
        comment = Comment.objects.first()
        url = 'http://localhost:8000/like/'
        data = {'comment':comment.id , 'like': False}
        client = APIClient()
        response2 = client.post(url, data, format='json')
        assert response2.status_code == 401  # post commment ok

    def test_comment_with_like(self):
        comment = Comment.objects.first()
        url = 'http://localhost:8000/comment/' + str(comment.id) +'/'
        response3 = self.client.get(url, format='json')
        #print(response3.content)
        assert response3.status_code == 200  # post commment ok

    def test_assrt_with_comment_with_like(self):
        comment = Comment.objects.first()
        url = 'http://localhost:8000/like/'
        data = {'comment': comment.id, 'like': False}
        response2 = self.client.post(url, data, format='json')
        client = APIClient()
        response3 = self.client.get('http://localhost:8000/assrt/'+str(self.assrt_id)+'/', format='json')
         #print(response3.content)
        assert response2.status_code == 202  # post commment ok

    def test_like_no_auth(self):

        like = Like.objects.first()
        client = APIClient()
        url = 'http://localhost:8000/like/'+ str(like.id) +'/'
        response = client.get(url,format='json')
        assert response.status_code == 200  # post commment ok

    def test_like_update(self):
        like = Like.objects.first()
        url = 'http://localhost:8000/like/'+ str(like.id) +'/'
        data= {'like':False}
        response = self.client.put(url,data = data, format='json')
        assert response.status_code == 201  # post commment ok

    def test_like_update_no_auth(self):
        like = Like.objects.first()
        url = 'http://localhost:8000/like/'+ str(like.id) +'/'
        client = APIClient()
        data= {'like':False}
        response = client.put(url,data = data, format='json')
        assert response.status_code == 401  # post commment ok

    def test_like_delete_no_auth(self):
        like = Like.objects.first()
        url = 'http://localhost:8000/like/'+ str(like.id) +'/'
        client = APIClient()
        response = client.delete(url,format='json')
        assert response.status_code ==  401  # post commment ok

    def test_like_delete(self):
        like = Like.objects.first()
        url = 'http://localhost:8000/like/'+ str(like.id) +'/'
        response = self.client.delete(url,format='json')
        assert response.status_code == 201  # post commment ok

