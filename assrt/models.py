from django.db import models
from slugger import AutoSlugField
from django.contrib.auth.models import User

class Assrt(models.Model):
    author = models.ForeignKey(User,on_delete=models.CASCADE, )
    text_assrt = models.CharField(max_length=200)
    description = models.CharField(max_length=500, null=True)
    approved = models.BooleanField(default=True)
    slug = AutoSlugField(populate_from='text_assrt', unique=True)
    logdatum = models.DateTimeField(auto_now_add=True)
    lang = models.CharField(max_length=15, default="en")
    share = models.FloatField(null=True)

    class Meta:
        ordering = ['-logdatum']

