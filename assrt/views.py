from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from assrt.models import Assrt
from assrt.serializers import AssrtSerializer, AssrtInsertSerializer
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from langdetect import detect
from drf_yasg.utils import swagger_auto_schema
from django.conf import settings



class AssrtViewSet(viewsets.ViewSet):
    """
    retrieve:
    Return the given assrt.

    create:
    Create a new assrt instance.
    """
    queryset = Assrt.objects.all()
    #lookup_field = 'slug'
    serializer_class = AssrtSerializer

    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    @swagger_auto_schema(operation_description="POST /assrt/", request_body=AssrtInsertSerializer)
    def create(self, request):
        if request.user.is_authenticated:
            data = JSONParser().parse(request)
            serializer = AssrtInsertSerializer(data=data)
            if serializer.is_valid():
                try:
                    lang = detect(data['text_assrt'] + "," + data["description"])
                except Exception as e:
                    lang = 'simple'
                lang_detect = settings.MAP_DICT.get(lang, 'simple')
                serializer.save(lang=lang_detect, author = request.user)
                return JsonResponse({'id': Assrt.objects.latest('id').id}, status=201, safe=False)
            return JsonResponse(serializer.errors, status=400)

    def retrieve(self, request, pk=None):
        queryset = Assrt.objects.all()
        assrt = get_object_or_404(queryset, id=pk)
        serializer = AssrtSerializer(assrt)
        return Response(serializer.data)


class AssrtWebViewSet(viewsets.ViewSet):
    """
    retrieve:
    Return the given assrt.

    """
    queryset = Assrt.objects.all()
    lookup_field = 'slug'
    serializer_class = AssrtSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

#    @swagger_auto_schema(operation_description="GET /assrt/", request_body=AssrtInsertSerializer)
    def retrieve(self, request, slug=None):
        queryset = Assrt.objects.all()
        assrt = get_object_or_404(queryset, slug=slug)
        serializer = AssrtSerializer(assrt)
        return Response(serializer.data)

