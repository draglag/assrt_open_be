from django.urls import path,include
from rest_framework.routers import SimpleRouter
from assrt import views

router = SimpleRouter()
router.register(r'assrt', views.AssrtViewSet)
router.register(r'web/assrt', views.AssrtWebViewSet)
urlpatterns = router.urls
