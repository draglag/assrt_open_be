from rest_framework import serializers
from assrt.models import Assrt
from comment.serializers import CommentInAssrtSerializer, CommentSerializer
from aut.serializers import JustUsernameSerializer

class AssrtInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assrt
        fields = ['text_assrt', 'description']

class AssrtSerializer(serializers.ModelSerializer):
    comments = CommentInAssrtSerializer(many=True, required=False)
    user_data = serializers.SerializerMethodField()

    def get_user_data(self, obj):
        user = JustUsernameSerializer(obj.author, many=False).data
        return user

    class Meta:
        model = Assrt
        fields = ['id','text_assrt', 'description','logdatum', 'user_data', 'comments']

class SlugAssrtSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assrt
        fields = ['text_assrt', 'slug']

class AssrtKeySerializer(serializers.Serializer):
    assrt_id = serializers.CharField(required=False, allow_blank=False, max_length=10)
    assrt_slug = serializers.CharField(required=False, allow_blank=False, max_length=200)
