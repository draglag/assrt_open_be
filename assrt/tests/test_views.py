from rest_framework.test import RequestsClient
from rest_framework.test import APITestCase, APIClient
from assrt.models import Assrt
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


class WithoutAuthTestCase(APITestCase):
    def setUp(self):
        user1 = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user1.save()
        user2 = User.objects.create_user(username='miao', email='miao@miao.com', password='miao')
        user2.save()
        ins=Assrt.objects.create(text_assrt= "silvio e corrotto", author = user1)
        ins.save()
        self.assrt_id = str(ins.id)
        ins2=Assrt.objects.create(text_assrt= "qualcuno e pulito", author = user2)
        ins2.save()

    def test_assrt_get(self):
        client = RequestsClient()
        response = self.client.get('http://localhost:8000/assrt/'+self.assrt_id+'/')
        assert response.status_code == 200

    def test_assrt_post_no_auth(self):
        url = 'http://localhost:8000/assrt/'
        data = {'text_assrt': 'test no auth'}
        response = self.client.post(url, data,format='json')
        assert response.status_code == 401

    def test_post_no_text_assrt(self):
        url = 'http://localhost:8000/assrt/'
        data = {'text_assrt': ''}
        response = self.client.post(url, data,format='json')
        assert response.status_code == 401

class WithAuthTestCase(APITestCase):
    def setUp(self):
        user1 = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user1.save()
        user2 = User.objects.create_user(username='miao', email='miao@miao.com', password='miao')
        user2.save()
        token1 = Token.objects.create(user=user1)
        token1.save()
        token2 = Token.objects.create(user=user2)
        token2.save()
        ins = Assrt.objects.create(text_assrt="silvio e corrotto", author = user1)
        ins.save()
        ins2 = Assrt.objects.create(text_assrt="qualcuno e pulito", author = user2)
        ins2.save()

    def test_post_assrt_basic_auth(self):
        client = APIClient()
        client.login(username='babu', password='bau')
        url = 'http://localhost:8000/assrt/'
        data = {'text_assrt': 'new idea for something'}
        response = client.post(url, data, format= 'json')
        assert response.status_code == 201

    def test_post_assrt_basic_auth_no_correct(self):
        client = APIClient()
        client.login(username='babu', password='bau')
        url = 'http://localhost:8000/assrt/'
        data = {'text_assrt': ''}
        response = client.post(url, data)
        assert response.status_code == 400

    def test_post_assrt_token_auth(self):
        user = User.objects.get(username='babu')
        token = Token.objects.get(user = user)
        auth_string = " Token {}".format(token.key)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION = auth_string)
        data = {'text_assrt': 'new idea for something'}
        response = client.post('/assrt/', data, format= 'json')
        assert response.status_code == 201

    def test_post_assrt_token_auth_no_correct(self):
        user = User.objects.get(username='babu')
        token = Token.objects.get(user=user)
        auth_string = " Token {}".format(token.key)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=auth_string)
        data = {'text_assrt': ''}
        response = client.post('/assrt/', data)
        assert response.status_code == 400
