from django.test import TestCase
from assrt.models import Assrt
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class AssrtTestCase(TestCase):

    def setUp(self):
        user1 = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user1.save()
        user2 = User.objects.create_user(username='miao', email='miao@miao.com', password='miao')
        user2.save()
        ins = Assrt.objects.create(text_assrt="silvio e corrotto", author=user1)
        ins.save()
        ins2 = Assrt.objects.create(text_assrt="qualcuno e pulito", author=user2)
        ins2.save()
        user = User.objects.get(username='babu')
        Token.objects.create(user=user)


    def test_getAll(self):
        res=Assrt.objects.all()
        assert res is not None

 #   def test_getId(self):
 #       silvio = Assrt.objects.get(text_assrt= "silvio e corrotto")
 #       qualcuno = Assrt.objects.get(text_assrt= "qualcuno e pulito")

#        self.assertEqual(silvio.id, 1)
#        self.assertEqual(qualcuno.id, 2)





