from rest_framework import serializers
from django.contrib.auth.models import User


class UsernameSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username', 'date_joined']

class JustUsernameSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','username',]
