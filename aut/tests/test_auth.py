from rest_framework.test import APITestCase, APIClient, RequestsClient
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from aut.serializers import JustUsernameSerializer
import json

client = RequestsClient()
"""
# Obtain a CSRF token.
response = client.get('http://testserver/homepage/')
assert response.status_code == 200
csrftoken = response.cookies['csrftoken']

# Interact with the API.
response = client.post('http://testserver/organisations/', json={
    'name': 'MegaCorp',
    'status': 'active'
}, headers={'X-CSRFToken': csrftoken})
assert response.status_code == 200
"""
class WithAuthTestCase(APITestCase):


    def setUp(self):
        user1 = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user1.save()
        token1 = Token.objects.create(user=user1)
        token1.save()


    def test_login_token(self):

        client = APIClient()
        user = User.objects.get(username='babu')
        token = Token.objects.get(user=user)
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        response = client.post('/assrt/', {'text_assrt': 'new idea for something'},  format='json')
        assert response.status_code == 201


#client = RequestsClient()


class TestTokenAuth(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user.save()
        self.client = RequestsClient()

    def test_new_user(self):

        params = {
            "username": "test_user",
            "email": "testmail@mail.com",
            "password1": "test_test_test",
            "password2": "test_test_test",
        }
        response = self.client.post('http://localhost:8000/auth/registration/', json= params)
        #print(response.content)
        content = json.loads(response.content)
        #print(content['access_token'])
        #self.content = content['access_token']
        assert response.status_code == 201

    def test_login(self):

        params1 = {
            "username": "test_user",
            "email": "testmail@mail.com",
            "password1": "test_test_test",
            "password2": "test_test_test",
        }
        response1 = self.client.post('http://localhost:8000/auth/registration/', json= params1)
        csrftoken = response1.cookies['csrftoken']
        params = {
            "username": "test_user",
            "password": "test_test_test"
        }
        response = self.client.post('http://localhost:8000/auth/login/', json= params, headers={'X-CSRFToken': csrftoken})
        #print(response.content)
        assert response.status_code == 200

    def test_serializer(self):
        user = User.objects.get(username = 'babu')
        serializer = JustUsernameSerializer(user)
        assert serializer.data['username'] == 'babu'
