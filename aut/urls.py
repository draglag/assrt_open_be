from django.urls import include, re_path, path
from aut.views import FacebookLogin, TwitterLogin
from dj_rest_auth.registration.views import VerifyEmailView
from django.conf.urls import include, url
from django.views.generic import RedirectView, TemplateView

urlpatterns = [
path('auth/', include('dj_rest_auth.urls')),
path('auth/registration/', include('dj_rest_auth.registration.urls')),
re_path(r'^facebook/$', FacebookLogin.as_view(), name='fb_login'),
re_path(r'^twitter/$', TwitterLogin.as_view(), name='twitter_login'),
    ]

