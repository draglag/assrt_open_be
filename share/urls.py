from rest_framework.routers import  SimpleRouter
from share import views

router = SimpleRouter()
router.register(r'share', views.ShareViewSet)
router.register(r'reputation', views.ReputationViewSet)
router.register(r'web/share', views.ShareWebViewSet)
router.register(r'web/reputation', views.ReputationWebViewSet)
urlpatterns = router.urls