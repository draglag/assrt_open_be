from like.models import Like
from assrt.models import Assrt
from comment.models import Comment
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
import json

class ShareTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user.save()
        user2 = User.objects.create_user(username='maio', email='miao@miao.com', password='miao')
        user2.save()
        ins=Assrt.objects.create(text_assrt= "silvio e corrotto", author = user)
        ins.save()
        comment = Comment(author=user,
                          assrt=ins,
                          text_comment='commmento like',
                          link= "www.google.it",
                          meta="meta desc",
                          yn=True)
        comment.save()

        comment2 = Comment(author=user,
                          assrt=ins,
                          text_comment='commmento no like',
                          link= "www.google.it",
                          meta="meta desc",
                          yn=False)
        comment2.save()
        like= Like(
            author= user,
            comment=comment,
            like= True
                )
        like.save()
        Token.objects.create(user=user)
        user = User.objects.get(username='babu')
        token = Token.objects.get(user=user)
        auth_string = " Token {}".format(token.key)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=auth_string)

    def test_share_view(self):
        url = 'http://localhost:8000/web/share/silvio-e-corrotto/'
        response = self.client.get(url, format='json')
        content = json.loads(response.content)
        #assert response.status_code == 201  # post commment ok
        assert content['share'] == 6/11

    def test_reputation_maio_view(self):
        url = 'http://localhost:8000/web/reputation/maio/'
        response = self.client.get(url, format='json')
        content = json.loads(response.content)
        #assert response.status_code == 201  # post commment ok
        assert content['reputation'] == 0


    def test_reputation_babu_view(self):
        url = 'http://localhost:8000/web/reputation/babu/'
        response = self.client.get(url, format='json')
        content = json.loads(response.content)
        #assert response.status_code == 201  # post commment ok
        assert content['reputation'] == 6/11
