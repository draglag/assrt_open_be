from django.http import JsonResponse
from rest_framework.viewsets import ViewSet
from django.shortcuts import get_object_or_404
from assrt.models import Assrt
from comment.models import Comment
from django.contrib.auth.models import User
from like.models import Like
import logging
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticatedOrReadOnly


logger = logging.getLogger(__name__)

class ShareViewSet(ViewSet):
    queryset = Assrt.objects.all()
    permission_classes = ([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = Assrt.objects.all()
        assrt = get_object_or_404(queryset, id=pk)
        comments = Comment.objects.filter(assrt = assrt)
        comments_pos = Comment.objects.filter(assrt=assrt, yn=True)
        like_counter = 0
        for comment in comments:
            count_like = Like.objects.filter(comment=comment).count()
            like_counter += count_like
        like_pos_counter = 0
        for comment in comments_pos:
            count_like_pos = Like.objects.filter(comment = comment).count()
            like_pos_counter += count_like_pos
        try:
            share = (comments_pos.count()*5+like_pos_counter)/(like_counter+ comments.count()*5)
        except ZeroDivisionError:
            share = 'no comments yet'
        return JsonResponse({'share': share}, status=201, safe=False)

class ShareWebViewSet(ViewSet):
    queryset = Assrt.objects.all()
    lookup_field = 'slug'
    permission_classes = ([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, slug=None):
        queryset = Assrt.objects.all()
        assrt = get_object_or_404(queryset, slug=slug)
        comments = Comment.objects.filter(assrt = assrt)
        comments_pos = Comment.objects.filter(assrt=assrt, yn=True)
        like_counter = 0
        for comment in comments:
            count_like = Like.objects.filter(comment=comment).count()
            like_counter += count_like
        like_pos_counter = 0
        for comment in comments_pos:
            count_like_pos = Like.objects.filter(comment = comment).count()
            like_pos_counter += count_like_pos
        try:
            share = (comments_pos.count()*5+like_pos_counter)/(like_counter+ comments.count()*5)
        except ZeroDivisionError:
            share = 'no comments yet'
        return JsonResponse({'share': share}, status=201, safe=False)

class ReputationViewSet(ViewSet):
    queryset = User.objects.all()
    permission_classes = ([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, id=pk)
        assrts = Assrt.objects.filter(author = user)
        #assrt made by user
        if assrts.count() == 0:
            return JsonResponse({'reputation': 0}, status=201, safe=False)
        else:
            for assrt in assrts:
                comments_pos = Comment.objects.filter(assrt=assrt).filter(yn = True)
                comments = Comment.objects.filter(assrt=assrt)
                like_pos_count = 0
                for comment in comments_pos:
                    likes_pos = Like.objects.filter(comment=comment).count()
                    like_pos_count += likes_pos
                like_count = 0
                for comment in comments:
                    likes_neg = Like.objects.filter(comment=comment).count()
                    like_count += likes_neg
            try:
                reputation = (comments_pos.count() * 5 + like_pos_count) / (like_count + comments.count() * 5)
            except ZeroDivisionError:
                reputation = 'the user was not active yet'
            return JsonResponse({'reputation': reputation}, status=201, safe=False)

class ReputationWebViewSet(ViewSet):
    queryset = User.objects.all()
    lookup_field = 'username'
    permission_classes = ([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, username=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, username=username)
        assrts = Assrt.objects.filter(author = user)
        #assrt made by user
        if assrts.count() == 0:
            return JsonResponse({'reputation': 0}, status=201, safe=False)
        else:
            for assrt in assrts:
                comments_pos = Comment.objects.filter(assrt=assrt).filter(yn = True)
                comments = Comment.objects.filter(assrt=assrt)
                like_pos_count = 0
                for comment in comments_pos:
                    likes_pos = Like.objects.filter(comment=comment).count()
                    like_pos_count += likes_pos
                like_count = 0
                for comment in comments:
                    likes_neg = Like.objects.filter(comment=comment).count()
                    like_count += likes_neg
            try:
                reputation = (comments_pos.count() * 5 + like_pos_count) / (like_count + comments.count() * 5)
            except ZeroDivisionError:
                reputation = 'the user was not active yet'
            return JsonResponse({'reputation': reputation}, status=201, safe=False)






