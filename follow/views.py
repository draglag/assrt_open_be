from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from follow.models import FollowUser, FollowAssrt
from django.contrib.auth.models import User
from assrt.models import Assrt
from follow.serializers import FollowUserSerializer, FollowAssrtSerializer, FollowUserWebSerializer, FollowAssrtWebSerializer
from drf_yasg.utils import swagger_auto_schema
from django.db.models import Q
import logging

logger = logging.getLogger(__name__)

# Create your views here.
class FollowUserViewSet(viewsets.ViewSet):
    queryset = FollowUser.objects.all()
    serializer_class = FollowUserSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = FollowUser.objects.all()
        follows = get_object_or_404(queryset, id=pk)
        #print(follows.id)
        #follows = Follow.objects.filter(cur_user = pk)
        serializer = FollowUserSerializer(follows)
        return Response(serializer.data)

    @swagger_auto_schema(operation_description="POST /follow-user/", request_body=FollowUserSerializer)
    def create(self, request):
        data = JSONParser().parse(request)
        serializer = FollowUserSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                if FollowUser.objects.\
                        filter(Q(follow=data['follow']) &
                               Q(cur_user=request.user)).\
                        count() == 0:
                    followed = User.objects.get(id=data['follow'])
                    follow = FollowUser(cur_user = request.user,
                                    follow = followed)
                    follow.save()
                    return JsonResponse({'id':follow.id}, status=201, safe=False)
                return JsonResponse({'message':'user already followed'}, status=202, safe=False)
            return JsonResponse(serializer.errors, status=400)


    def destroy(self, request, pk=None):
        if request.user.is_authenticated:
            follow = FollowUser.objects.get(id=pk)
            if follow.cur_user == request.user:
                follow.delete()
                return JsonResponse("ok", status=201, safe=False)
            else:
                return JsonResponse("error", status=400, safe=False)

class FollowUserWebViewSet(viewsets.ViewSet):
    queryset = FollowUser.objects.all()
    lookup_field = 'username'
    serializer_class = FollowUserSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, username=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, username=username)
        follows = FollowUser.objects.filter(cur_user_id=user.id)
        serializer = FollowUserWebSerializer(follows, many=True)
        return Response(serializer.data)

class FollowAssrtViewSet(viewsets.ViewSet):
    queryset = FollowAssrt.objects.all()
    serializer_class = FollowAssrtSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = FollowAssrt.objects.all()
        assrts = get_object_or_404(queryset, id=pk)
        #print(follows.id)
        #follows = Follow.objects.filter(cur_user = pk)
        serializer = FollowAssrtSerializer(assrts)
        return Response(serializer.data)

    @swagger_auto_schema(operation_description="POST /follow-assrt/", request_body=FollowAssrtSerializer)
    def create(self, request):
        data = JSONParser().parse(request)
        serializer = FollowAssrtSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                if FollowAssrt.objects.filter(id = data['assrt'], cur_user=request.user).count() == 0:
                    assrt = Assrt.objects.get(id= data['assrt'])
                    follow = FollowAssrt(cur_user = request.user,
                                        assrt = assrt)
                    follow.save()
                    return JsonResponse({'id':follow.id}, status=201, safe=False)
                return JsonResponse({'message': 'assrt already followed'}, status=202, safe=False)
            return JsonResponse(serializer.errors, status=400)


    def destroy(self, request, pk=None):
        if request.user.is_authenticated:
            follow = FollowAssrt.objects.get(id=pk)
            if follow.cur_user == request.user:
                follow.delete()
                return JsonResponse("ok", status=201, safe=False)
            else:
                return JsonResponse("error", status=400, safe=False)

class FollowAssrtWebViewSet(viewsets.ViewSet):
    queryset = FollowAssrt.objects.all()
    lookup_field = 'username'
    serializer_class = FollowAssrtSerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, username=None):
        queryset2 = User.objects.all()
        user = get_object_or_404(queryset2, username=username)
        follows = FollowAssrt.objects.filter(cur_user_id=user.id)
        serializer = FollowAssrtWebSerializer(follows, many=True)
        return Response(serializer.data)
