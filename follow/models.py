from django.db import models
from django.contrib.auth.models import User
from assrt.models import Assrt

class FollowUser(models.Model):
    cur_user = models.ForeignKey(User,related_name='cur_user', on_delete=models.CASCADE )
    follow = models.ForeignKey(User,related_name='follow', on_delete= models.CASCADE)
    #follow = models.CharField(max_length=100)
    logdatum = models.DateTimeField(auto_now_add=True)

class FollowAssrt(models.Model):
    cur_user = models.ForeignKey(User,related_name='cur_assrt', on_delete=models.CASCADE )
    assrt = models.ForeignKey(Assrt, related_name='assrt', on_delete= models.CASCADE)
    #slug = models.CharField(max_length=100)
    logdatum = models.DateTimeField(auto_now_add=True)


