from rest_framework import serializers
from follow.models import FollowUser, FollowAssrt
from aut.serializers import UsernameSerializer
from assrt.serializers import SlugAssrtSerializer

class FollowUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = FollowUser
        fields = ('follow_id',)

class FollowAssrtSerializer(serializers.ModelSerializer):
    class Meta:
        model = FollowAssrt
        fields = ('assrt',)


class FollowUserWebSerializer(serializers.ModelSerializer):
    follow = UsernameSerializer()

    class Meta:
        model = FollowUser
        fields = ['id','follow_id','follow']


class FollowAssrtWebSerializer(serializers.ModelSerializer):
    assrt = SlugAssrtSerializer()

    class Meta:
        model = FollowAssrt
        fields = ['id','assrt_id','assrt']
