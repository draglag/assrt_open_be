from rest_framework.routers import SimpleRouter
from follow import views

router = SimpleRouter()
router.register(r'follow-user', views.FollowUserViewSet)
router.register(r'follow-assrt', views.FollowAssrtViewSet)

router.register(r'web/follow-user', views.FollowUserWebViewSet)

router.register(r'web/follow-assrt', views.FollowAssrtWebViewSet)
urlpatterns = router.urls