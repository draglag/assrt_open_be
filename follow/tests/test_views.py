from follow.models import FollowUser, FollowAssrt
from rest_framework.test import APIClient, APITestCase
from django.contrib.auth.models import User
from assrt.models import Assrt
from rest_framework.authtoken.models import Token

class FollowUserTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user.save()
        self.user= str(user.id)
        user2 = User.objects.create_user(username='miao', email='miao@miao.com', password='bau')
        user2.save()
        self.user2= str(user2.id)
        user3 = User.objects.create_user(username='cip', email='cip@miao.com', password='bau')
        user3.save()
        self.user3= str(user3.id)
        Token.objects.create(user=user)
        user = User.objects.get(username='babu')
        user2 = User.objects.get(username ='miao')
        token = Token.objects.get(user=user)
        follow = FollowUser(cur_user = user,
                         follow = user2)
        follow.save()
        self.follow = str(follow.id)
        auth_string = " Token {}".format(token.key)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=auth_string)

    def test_follow(self):
        user3 = User.objects.get(id = self.user3)
        url = 'http://localhost:8000/follow-user/'
        data = {'follow':user3.id }
        response2 = self.client.post(url, data, format='json')
        assert response2.status_code == 201  # post commment ok


    def test_follow_400(self):
        user2 = User.objects.get(id = self.user2)
        url = 'http://localhost:8000/follow-user/'
        data = {'follow':user2.id }
        response2 = self.client.post(url, data, format='json')
        assert response2.status_code == 202  # post commment ok

    def test_follow_retrieve(self):
        user = User.objects.get(username = 'babu')
        url = 'http://localhost:8000/follow-user/'+self.follow+"/"
        response2 = self.client.get(url,format='json')
        assert response2.status_code == 200  # post commment ok

    def test_follow_delete(self):
        follow = FollowUser.objects.first()
        url = 'http://localhost:8000/follow-user/' + str(follow.id) + '/'
        response = self.client.delete(url, format='json')
        assert response.status_code == 201  # post commment ok

class FollowAssrtTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user.save()
        Token.objects.create(user=user)
        token = Token.objects.get(user=user)
        ins = Assrt.objects.create(text_assrt="silvio e corrotto",
                                   author=user)
        ins.save()

        ins2 = Assrt.objects.create(text_assrt="silvio e pulito",
                                   author=user)
        ins2.save()
        follow = FollowAssrt(cur_user=user,
                             assrt= ins)
        follow.save()
        self.follow=str(follow.id)
        auth_string = " Token {}".format(token.key)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=auth_string)

    def test_exists_follow(self):
        assrt = Assrt.objects.get(slug="silvio-e-corrotto")
        user = User.objects.get(username="babu")
        follow= FollowAssrt.objects.get(assrt=assrt)
        assert follow.cur_user == user
        assert follow.assrt == assrt

    def test_follow(self):
        assrt = Assrt.objects.get(slug='silvio-e-pulito')
        url = 'http://localhost:8000/follow-assrt/'
        data = {'assrt': assrt.id}
        response2 = self.client.post(url, data, format='json')
        assert response2.status_code == 201  # post commment ok

    def test_follow_400(self):
        assrt = Assrt.objects.get(slug='silvio-e-corrotto')
        url = 'http://localhost:8000/follow-assrt/'
        data = {'slug': assrt.id}
        response2 = self.client.post(url, data, format='json')
        assert response2.status_code == 400  # post commment ok

    def test_follow_202(self):
        assrt = Assrt.objects.get(slug='silvio-e-corrotto')
        url = 'http://localhost:8000/follow-assrt/'
        data = {'assrt': assrt.id}
        response2 = self.client.post(url, data, format='json')
        print(response2.status_code)
        assert response2.status_code == 202  # post commment ok

    def test_follow_retrieve(self):
        user = User.objects.get(username='babu')
        url = 'http://localhost:8000/follow-assrt/' + self.follow + "/"
        response2 = self.client.get(url, format='json')
        assert response2.status_code == 200  # post commment ok

    def test_follow_delete(self):
        follow = FollowAssrt.objects.first()
        url = 'http://localhost:8000/follow-assrt/' + str(follow.id) + '/'
        response = self.client.delete(url, format='json')
        assert response.status_code == 201  # post commment ok

