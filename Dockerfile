# The first instruction is what image we want to base our container on
# We Use an official Python runtime as a parent image
FROM python:3.6-slim-stretch

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y libpq-dev python3-dev gcc

# create root directory for our project in the container
RUN mkdir /django-assrt
COPY . /django-assrt/

WORKDIR /django-assrt
RUN pip install -r requirements.txt
