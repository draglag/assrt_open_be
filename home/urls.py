from rest_framework.routers import SimpleRouter
from home import views

router = SimpleRouter()
router.register(r'home', views.HomeViewSet)
urlpatterns = router.urls