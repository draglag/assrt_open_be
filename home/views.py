from django.http import  JsonResponse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.decorators import action
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from assrt.models import Assrt
from assrt.serializers import AssrtSerializer
from home.serializers import  TenSerializer, LangSerializer
from django.db.models import Count
import logging
from rest_framework.permissions import AllowAny
from drf_yasg.utils import swagger_auto_schema
from django.conf import settings

logger = logging.getLogger(__name__)

class HomeViewSet(viewsets.ViewSet):

    queryset = Assrt.objects.all()
    serializer_class = AssrtSerializer
    permission_classes = ([AllowAny])

    @swagger_auto_schema(operation_description="POST /newten/", request_body=LangSerializer)
    @action(detail=False, methods=['POST'])
    def newten(self, request):
        data = JSONParser().parse(request)
        lang_serializer = LangSerializer(data=data)
        if lang_serializer.is_valid():
            recent_assrts = Assrt.objects\
                                .filter(lang=data['lang'], approved=True)\
                                .order_by('-logdatum')[:10]
            serializer = TenSerializer(recent_assrts, many=True)
        return Response(serializer.data, status=201)

    @swagger_auto_schema(operation_description="POST /topten/", request_body=LangSerializer)
    @action(detail=False, methods=['POST'])
    def topten(self, request):
        data = JSONParser().parse(request)
        lang_serializer = LangSerializer(data=data)
        if lang_serializer.is_valid():
            top_ten = Assrt.objects.annotate(num_comments=Count('comments')) \
                          .filter(lang = settings.MAP_DICT[data['lang']], approved=True) \
                          .order_by('-num_comments')[:50]
                          #.filter(lang = map_dict[data['lang']], approved=True) \
            serializer = TenSerializer(top_ten, many=True)
        return Response(serializer.data, status=201)