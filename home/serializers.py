from rest_framework import serializers
from assrt.models import Assrt
from comment.serializers import CommentCountSerializer

class AssrtSearchSerializer(serializers.ModelSerializer):
    comments = CommentCountSerializer(many=True, read_only=True)

    class Meta:
        model = Assrt
        fields = ['text_assrt', 'slug', 'comments']

class TenSerializer(serializers.ModelSerializer):
    comments = CommentCountSerializer(many=True, read_only=True)

    class Meta:
        model = Assrt
        fields = ['text_assrt', 'slug', 'comments']
        # fields = "__all__"

class LangSerializer(serializers.Serializer):
    lang = serializers.CharField(max_length=20)