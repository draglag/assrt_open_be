from assrt.models import Assrt
from rest_framework.test import  APIClient
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import RequestsClient
from rest_framework.test import APITestCase
import  assrt_be.settings as s
import json
#local server have to run

class TestViews(APITestCase):

    def setUp(self):
        user1 = User.objects.create_user(username='babu', email='bau@bau.com',password='bau')
        user1.save()
        user2 = User.objects.create_user(username='miao', email='miao@miao.com', password='miao')
        user2.save()
        ins = Assrt.objects.create(text_assrt="silvio e corrotto", lang = 'english', author=user1, approved=True)
        ins.save()
        ins2 = Assrt.objects.create(text_assrt="nuova idea da testare", lang = 'english',  author=user2, approved=True)
        ins2.save()
        self.client = APIClient()

    def test_new_ten_post_italian(self):
        data = {'lang':'it'}
        response = self.client.post('/home/newten/', data = data, format='json')
        assert response.status_code == 201
        assert len(response.content) == 2

    def test_new_ten_post_english(self):
        data = {'lang':'en'}
        response = self.client.post('/home/newten/', data = data, format='json')
        assert response.status_code == 201
        #assert len(response.content) == 161

    def test_top_ten_post_italian(self):
        data = {'lang':'it'}
        response = self.client.post('/home/topten/', data = data, format='json')
        assert response.status_code == 201
        assert len(response.content) == 2

    def test_top_ten_post_english(self):
        data = {'lang':'en'}
        response = self.client.post('/home/topten/', data = data, format='json')
        assert response.status_code == 201
        assert len(response.content) == 161

"""        
    def test_top_ten_with_comment(self):
        if s.FIREWALL:
            pass
        else:
            url = 'http://localhost:8000/assrt/'
            response1 = self.client.post(url, {'text_assrt': 'new idea'}, format='json')
            print(response1.status_code)
            response6 = self.client.get('http://localhost:8000/assrt/new-idea/', format='json')
            print(response6.json())
            data = {'slug': 'new-idea',
                    'text_comment': 'test no link',
                    'link': 'https://stackoverflow.com/',
                    'yn': 'False'}
            response2 = self.client.post('http://localhost:8000/comment/', data, format='json')
            response3 = self.client.post(url, {'text_assrt': 'new idea 1'}, format='json')
            response4 = self.client.post(url, {'text_assrt': 'new idea 2'}, format='json')
            print(response2.status_code)
            response = self.client.get('http://localhost:8000/home/topten/', format='json')
            print(response.json())
            assert response.status_code == 201


    def test_new_ten(self):
        url = 'http://localhost:8000/assrt/'
        response = self.client.post(url, {'text_assrt': 'new idea'}, format='json')
        response = self.client.post(url, {'text_assrt': 'new idea 1'}, format='json')
        response = self.client.post(url, {'text_assrt': 'new idea 2'}, format='json')
        response = self.client.get('http://localhost:8000/home/newten/', format='json')
        assert response.status_code == 201
        assert len(response.content) == 183

    def test_search_accept_request(self):
        url1 = 'http://localhost:8000/assrt/'
        data1 = {'text_assrt': 'new'}
        response1 = self.client.post(url1, data1, format='json')
        url = 'http://localhost:8000/search/'
        data = {'search_text' : 'new'}
        response = self.client.post(url, data, format='json')
        assert response.status_code == 201

    def test_search_accept_request(self):
        url1 = 'http://localhost:8000/assrt/'
        data1 = {'text_assrt': 'new'}
        response1 = self.client.post(url1, data1, format='json')
        url = 'http://localhost:8000/search/'
        data = {'search_text': ''}
        response = self.client.post(url, data, format='json')
        assert response.status_code == 400
"""