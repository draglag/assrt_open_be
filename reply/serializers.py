from rest_framework import serializers
from reply.models import Reply
from aut.serializers import JustUsernameSerializer

class ReplySerializer(serializers.ModelSerializer):
    class Meta:
        model = Reply
        fields = ('text_reply',)

class ReplyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reply
        fields = ('comment', 'text_reply')

class ReplyInCommenmentSerializer(serializers.ModelSerializer):
    user_data = serializers.SerializerMethodField()

    def get_user_data(self, obj):
        user = JustUsernameSerializer(obj.author , many=False).data
        return user

    class Meta:
        model = Reply
        fields = ('author','text_reply', 'logdatum', 'user_data')