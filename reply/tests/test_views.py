from assrt.models import Assrt
from comment.models import Comment
from reply.models import Reply
from rest_framework.test import  APIClient, APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class ReplyTestCase(APITestCase):
    def setUp(self):
        user1 = User.objects.create_user(username='babu', email='bau@bau.com', password='bau')
        user1.save()
        user2 = User.objects.create_user(username='miao', email='miao@miao.com', password='miao')
        user2.save()
        ins=Assrt.objects.create(text_assrt= "silvio e corrotto", author = user1)
        ins.save()
        self.assrt_id = ins.id
        ins2=Assrt.objects.create(text_assrt= "nuova idea da testare", author = user2)
        ins2.save()
        comment = Comment.objects.create(author=user1,
                                         assrt=ins,
                                         text_comment="commento di test",
                                         link="http://www.google.it",
                                         meta="meta descritpion",
                                         yn=False)
        comment.save()
        reply = Reply(author=user1,
                       comment = comment,
                       text_reply= "vero vero vero")
        reply.save()
        self.reply_id = str(reply.id)
        user = User.objects.get(username='babu')
        Token.objects.create(user=user)
        user = User.objects.get(username='babu')
        token = Token.objects.get(user=user)
        auth_string = " Token {}".format(token.key)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=auth_string)
        user2 = User.objects.get(username='miao')
        Token.objects.create(user=user2)
        token2 = Token.objects.get(user=user2)
        auth_string2 = " Token {}".format(token2.key)
        self.client2 = APIClient()
        self.client2.credentials(HTTP_AUTHORIZATION=auth_string2)

    def test_reply_200(self):
        response3 = self.client.get('http://localhost:8000/reply/'+self.reply_id+'/', format='json')
        assert response3.status_code == 200

    def test_reply_in_comment_in_assrt_200(self):
        response3 = self.client.get('http://localhost:8000/assrt/'+str(self.assrt_id)+'/', format='json')
        assert len(response3.data["comments"][0]['reply'])==1

    def test_reply_update(self):
        url = 'http://localhost:8000/reply/'+self.reply_id+'/'
        data = {'text_reply': 'qualcosaltor'}
        response = self.client.put(url, data=data, format='json')
        assert response.status_code == 201  # post commment ok

    def test_prof_reply_no_auth(self):
        client = APIClient()
        url = 'http://localhost:8000/reply/'+self.reply_id+'/'
        data = {'bio': 'qualcosaltor'}
        response = client.put(url, data=data, format='json')
        assert response.status_code == 401  # post commment ok

    def test_reply_delete_no_auth(self):
        client = APIClient()
        url = 'http://localhost:8000/reply/'+self.reply_id+'/'
        response = client.delete(url, format='json')
        assert response.status_code == 401  # post commment ok

    def test_reply_delete(self):
        url = 'http://localhost:8000/reply/'+self.reply_id+'/'
        response = self.client.delete(url, format='json')
        assert response.status_code == 201  # post commment ok

