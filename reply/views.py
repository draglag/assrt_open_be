from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from reply.models import Reply
from comment.models import Comment
from reply.serializers import ReplySerializer, ReplyCreateSerializer
import logging
from drf_yasg.utils import swagger_auto_schema

logger = logging.getLogger(__name__)

# Create your views here.
class ReplyViewSet(viewsets.ViewSet):
    """
    retrieve:
    Return the given reply.

    create:
    Insert a new reply.

    update:
    Update a reply.

    destroy:
    delete a reply.
    """
    queryset = Reply.objects.all()
    serializer_class = ReplySerializer
    permission_classes =([IsAuthenticatedOrReadOnly])
    authentication_classes = [TokenAuthentication, BasicAuthentication, SessionAuthentication]

    def retrieve(self, request, pk=None):
        queryset = Reply.objects.all()
        comments = get_object_or_404(queryset, id=pk)
        serializer = ReplySerializer(comments)
        return Response(serializer.data)

    @swagger_auto_schema(operation_description="POST /reply/", request_body=ReplyCreateSerializer)
    def create(self, request):
        data = JSONParser().parse(request)
        serializer = ReplyCreateSerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                comment = Comment.objects.get(id=data['comment'])
                reply = Reply(author=request.user,
                              comment=comment,
                              text_reply=data['text_reply'],
                              )
                reply.save()
                return JsonResponse({'id': reply.id}, status=201, safe=False)
            return JsonResponse(serializer.errors, status=400)

    @swagger_auto_schema(operation_description="UPDATE /reply/{id}/", request_body=ReplySerializer)
    def update(self, request, pk=None):
        data = JSONParser().parse(request)
        serializer = ReplySerializer(data=data)
        if request.user.is_authenticated:
            if serializer.is_valid():
                reply = Reply.objects.get(id=pk)
                if reply.author == request.user:
                    reply.text_reply = data['text_reply']
                    reply.save()
                    return JsonResponse("ok",status=201, safe=False)
                else:
                    return JsonResponse("author error", status=400, safe=False)

    def destroy(self, request, pk=None):
        if request.user.is_authenticated:
            reply = Reply.objects.get(id=pk)
            #print("{0}--{1}".format(comment.author, request.user))
            if reply.author == request.user:
                reply.delete()
                return JsonResponse("ok",status=201, safe=False)
            else:
                return JsonResponse("author error", status=400, safe=False)