from rest_framework.routers import SimpleRouter
from reply import views

router = SimpleRouter()
router.register(r'reply', views.ReplyViewSet)
urlpatterns = router.urls
