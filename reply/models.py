from django.db import models
from django.conf import settings
from comment.models import Comment

# Create your models here.
class Reply(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,  on_delete=models.CASCADE, )
    comment = models.ForeignKey(Comment, null = True, related_name='reply',on_delete=models.CASCADE)
    text_reply = models.CharField(max_length=200, blank=True)
    approved = models.BooleanField(default=True)
    logdatum = models.DateTimeField(auto_now_add=True)
